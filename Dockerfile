FROM training/webapp

MAINTAINER Noam Y. Tenne <noam@10ne.org>
CMD python app.py
